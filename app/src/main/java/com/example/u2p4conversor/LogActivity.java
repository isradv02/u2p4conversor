package com.example.u2p4conversor;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends AppCompatActivity {
    private static String DEBUG_TAG = " ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        String eventName = "onCreate";
        Log.i(DEBUG_TAG, eventName);
    }

    public void setDEBUG_TAG(String clase) {
        this.DEBUG_TAG = "LOG-"+clase;
    }

    @Override
    protected void onStart() {
        super.onStart();
        String eventName = "onStart";
        Log.i(DEBUG_TAG, eventName);
    }

    @Override
    protected void onStop() {
        super.onStop();
        String eventName = "onStop";
        Log.i(DEBUG_TAG, eventName);
    }

    @Override
    protected void onPause() {
        super.onPause();
        String eventName = "onPause";
        Log.i(DEBUG_TAG, eventName);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String eventName = "onResume";
        Log.i(DEBUG_TAG, eventName);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        String eventName = "onRestart";
        Log.i(DEBUG_TAG, eventName);
        setDEBUG_TAG(this.getClass().getSimpleName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        String whois = (isFinishing())?"USER":"SYSTEM";
        String eventName = "onDestroy";
        Log.i(DEBUG_TAG, eventName+", destroyed by: "+whois);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String eventName = "onRestoreIstanceState";
        Log.i(DEBUG_TAG, eventName);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        String eventName = "onSaveIstanceState";
        Log.i(DEBUG_TAG, eventName);
    }
}