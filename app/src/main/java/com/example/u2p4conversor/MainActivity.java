package com.example.u2p4conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
// TODO: Ex3 Logs
public class MainActivity extends LogActivity {
    // TODO: Ex4 Maintain UI
    private static String strError = "";
    private static TextView etError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // TODO: Ex3 Logs
        super.setDEBUG_TAG(this.getClass().getSimpleName());
        setUI();
    }

    // TODO: Ex4 Maintain UI
    @Override
    protected void onStop() {
        super.onStop();
        strError = etError.getText().toString();
    }

    // TODO: Ex4 Maintain UI
    @Override
    protected void onResume() {
        super.onResume();
        etError.setText(strError);
    }

    private void setUI() {
        EditText etPulgada = findViewById(R.id.txt_pulgadas);
        EditText etCentimetros = findViewById(R.id.txt_centimetros);
        Button btConvertirACent = findViewById(R.id.btConvertirACent);
        // TODO: Ex1 Two-way conversion inch <-> cm
        Button btConvertirAPulgs = findViewById(R.id.btConvertirAPulgs);
        // TODO: Ex2 Exceptions
        etError = findViewById(R.id.txt_error);
        etError.setText("");


        // TODO: Ex1 Two-way conversion inch <-> cm
        btConvertirAPulgs.setOnClickListener(view -> {
            try{
                // TODO: Ex3 Logs
                Log.i("LOG-"+this.getClass().getSimpleName(),"Apretado botón a Pulgadas");
                etPulgada.setText(convertirAPulgadas(etCentimetros.getText().toString()));
                // TODO: Ex2 Exceptions
                etError.setText("");
            } catch (Exception e){
                Log.e("LogsConversor", e.getMessage());
                // TODO: Ex2 Exceptions
                etError.setText(e.getMessage());
            }

        });

        btConvertirACent.setOnClickListener(view -> {
            try{
                // TODO: Ex3 Logs
                Log.i("LOG-"+this.getClass().getSimpleName(),"Apretado botón a Centímetros");
                etCentimetros.setText(convertirACentimetros(etPulgada.getText().toString()));
                // TODO: Ex2 Exceptions
                etError.setText("");
            } catch (Exception e){
                Log.e("LogsConversor", e.getMessage());
                // TODO: Ex2 Exceptions
                etError.setText(e.getMessage());
            }

        });
    }


    private String convertirACentimetros(String num) throws Exception {
        Double numero = Double.parseDouble(num);
        // TODO: Ex2 Exceptions
        if(numero < 1) throw new Exception("Sólo números >=1");
        double pulgadaValue = Math.round((numero*2.54)*100.0)/100.0;
        return String.valueOf(pulgadaValue);
    }
    // TODO: Ex1 Two-way conversion inch <-> cm
    private String convertirAPulgadas(String num) throws Exception {
        Double numero = Double.parseDouble(num);
        // TODO: Ex2 Exceptions
        if(numero < 1) throw new Exception("Sólo números >=1");
        double pulgadaValue = Math.round((numero/2.54)*100.0)/100.0;
        return String.valueOf(pulgadaValue);
    }

}